# Lab 3 - Brincando la validación del password

A veces necesitamos analizar un binario para descubrir constraseñas escondidas o para esquivar la autenticación de contraseñas.  De esta forma podemos acceder a la funcionalidad que el fabricante nos está limitando por medio de contraseñas.

Para analizar un binario y poder extraer la información que nos permita lograr nuestro propósito tenemos al menos dos posibles herramientas:

* desensambladores: analizan el archivo binario y convierten código de máquina en lenguaje de ensamblaje. Desventaja: usados por si solos requieren que *corras* en tu cabeza el código. Esta labor se dificulta ya que el código que no tiene variables, comentarios y utiliza todo tipo de formas extrañas para lograr lo que normalmente realizas en C/C++. 

* debuggers: en adición a ver el código desensamblado, podemos detener el programa en la instrucción que se nos plazca, leer/escribir a registros/memoria, cambiar el flujo de ejecución del programa. 

En este lab usarás un desensamblador (`objdump -d`) y debugger (`ddd`) para determinar el password de dos ejectuables. 


### Ejercicio 1

En este ejercicio buscarás la contraseña que ha sido *hardcoded* en el programa. Usa alguno de los tools que te permiten ver el contenido de la sección .rodata. 

Corre el programa y usa la constraseña que averiguaste. Boom!


### Ejercicio 2

Solo un verdadero `0xbaca1a0` usaría un password *hardcoded* en su programa. Los programas con algun tipo de protección decente encriptan el password o lo ofuscan escondiéndolo entre millones de otras cosas que harían su detección bien trabajosa.

El ejecutable `pass02` utiliza un metodo de ofuscación elemental para dificultar la búsqueda de la contraseña.  En lugar de tratar de entender el algoritmo de ofuscación, debes utilizar tus destrezas de RE para hallar la parte del programa donde se están comparando la contraseña que entró el usuario vs. la contraseña correcta.

Sugerencia: la comparación entre el password provisto por el usuario y el password correcto sucede mediante una invocación a la función `strcmp`. 

El `main` de este programa es largo y puede que `ddd` no lo muestre completo en el `Machine Code Window`. Si deseas verlo completo usa el comando `disas` u obtén un listado del código usando `objdump -d nombreDelEjecutable`. 

### Ejercicio 3

Usando `hexedit` o tu herramienta predilecta de modificación de binarios, modifica el código `pass03` para que cualquier password entrado por el usuario le permita llegar al placer.

 
A modo de ejemplo, observa la dirección donde reside el `test   %eax,%eax` en este programa: 

```
  4006e2:    48 89 c7                 mov    %rax,%rdi
  4006e5:    e8 96 fe ff ff           callq  400580 <strcmp@plt>
  4006ea:    85 c0                    test   %eax,%eax
  4006ec:    74 0c                    je     4006fa <main+0x5d>
```

La seccion de texto comienza casi siempre en 0x400000 por lo que dicha instruccion tiene un offset de 0x6ea.

En hexedit las direcciones de las instrucciones aparecen con una dirección base de 0. Por lo tanto, se supone que encontremos el código de la instrucción `test   %eax,%eax` en la dirección `0x6ea`.

```
000006E4   C7 E8 96 FE  FF FF 85 C0  74 0C BF CF  07 40 00 E8  48 FE FF FF
^^^^^^^^                      ^^^^^
direccion del                 bytes que se encuentran en
primer byte de                0x6eA y 0x6eB: codigo de la
esta linea                    instruccion test %eax,%eax
```


### Entregables

A través de Moodle, somete un documento describiendo cómo encontraste los passwords de la parte 1 y 2. Por amor a tu deidad favorita, somete un documento digno de un estudiante avanzado de CCOM. Si deseas capturar pantallazos (screenshots) en Ubuntu, recomiendo el programa `shutter` (
`sudo apt-get install shutter`) y google docs.

Debes usar tipo de letra monoespaciado (*fixed width font*, e.g. courier, monaco) para listados de código.
